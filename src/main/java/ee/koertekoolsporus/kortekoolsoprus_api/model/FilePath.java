package ee.koertekoolsporus.kortekoolsoprus_api.model;

public class FilePath {
     private String generatedFileName;
    private String targetLocation;

    public FilePath(String generatedFileName, String targetLocation) {
        this.generatedFileName = generatedFileName;
        this.targetLocation = targetLocation;
    }

    public FilePath() {

    }

    public String getGeneratedFileName() {
        return generatedFileName;
    }

    public void setGeneratedFileName(String generatedFileName) {
        this.generatedFileName = generatedFileName;
    }

    public String getTargetLocation() {
        return targetLocation;
    }

    public void setTargetLocation(String targetLocation) {
        this.targetLocation = targetLocation;
    }
}
