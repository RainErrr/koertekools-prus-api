package ee.koertekoolsporus.kortekoolsoprus_api.model;

import java.util.List;

public class Article {
    private int id;
    private String heading;
    private String intro;
    private String body;
    private int type;
    private int focusId;
    private boolean active;
    private List<FilePath> files;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public String getIntro() {
        return intro;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getFocusId() {
        return focusId;
    }

    public void setFocusId(int focusId) {
        this.focusId = focusId;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<FilePath> getFiles() {
        return files;
    }

    public void setFiles(List<FilePath> files) {
        this.files = files;
    }

    public Article(int id, String heading, String intro, String body, int type, int focusId, boolean active) {
        this.id = id;
        this.heading = heading;
        this.intro = intro;
        this.body = body;
        this.type = type;
        this.focusId = focusId;
        this.active = active;
    }

    public Article() {

    }
}
