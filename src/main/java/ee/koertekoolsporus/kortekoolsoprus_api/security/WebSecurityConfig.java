package ee.koertekoolsporus.kortekoolsoprus_api.security;

import ee.koertekoolsporus.kortekoolsoprus_api.service.JwtUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private JwtUserDetailsService jwtUserDetailsService;
    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;
    @Autowired
    private JwtRequestFilter jwtRequestFilter;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
//                .antMatchers("/users/register").permitAll()
//                .antMatchers("/users/login").permitAll()
//                .antMatchers("/files/file/**").permitAll()
//                .antMatchers(HttpMethod.GET, "/articles**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.GET, "/articles/active/**").permitAll()
//                .antMatchers(HttpMethod.POST, "/articles**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PUT, "/articles**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.DELETE, "/articles/**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.GET, "/files**").hasAnyRole("ADMIN", "USER")
//                .antMatchers(HttpMethod.POST, "/files**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.PUT, "/files**").hasRole("ADMIN")
//                .antMatchers(HttpMethod.DELETE, "/files/**").hasRole("ADMIN").and()
                .antMatchers("/users/register").permitAll()
                .antMatchers("/users/login").permitAll()
                .antMatchers("/files/file/**").permitAll()
                .antMatchers(HttpMethod.GET, "/articles**").permitAll()
                .antMatchers(HttpMethod.GET, "/articles/active/**").permitAll()
                .antMatchers(HttpMethod.POST, "/articles**").permitAll()
                .antMatchers(HttpMethod.PUT, "/articles**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/articles/**").permitAll()
                .antMatchers(HttpMethod.GET, "/files**").permitAll()
                .antMatchers(HttpMethod.POST, "/files**").permitAll()
                .antMatchers(HttpMethod.PUT, "/files**").permitAll()
                .antMatchers(HttpMethod.DELETE, "/files/**").permitAll().and()
                .exceptionHandling()
                .authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http.csrf().disable();
        http.headers().frameOptions().disable();
        http.cors();
        http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }
}


