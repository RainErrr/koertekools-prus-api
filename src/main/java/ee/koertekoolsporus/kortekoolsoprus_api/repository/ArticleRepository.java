package ee.koertekoolsporus.kortekoolsoprus_api.repository;

import ee.koertekoolsporus.kortekoolsoprus_api.model.Article;
import ee.koertekoolsporus.kortekoolsoprus_api.model.FilePath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ArticleRepository {

    private  String INSERT_SQL = "insert into articles (heading, intro, body, contentType, focusId, active) values(:heading, :intro, :body, :contentType, :focusId, :active)";

    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private FileRepository fileRepository;


    public List<Article> fetchArticles() {
        return jdbcTemplate.query("Select * from articles", (row, number) -> {
            Article a = new Article(
                    row.getInt("id"), row.getString("heading"), row.getString("intro"),
                    row.getString("body"), row.getInt("contentType"), row.getInt("focusId"), row.getBoolean("active")
            );
            List<FilePath> files = fileRepository.getFilesForArticle(row.getInt("id"));
            a.setFiles(files);
            return a;
        });
    }

    public List<Article> fetchActiveArticles(boolean active) {
        return jdbcTemplate.query("Select * from articles where active = 1", (row, number) -> {
            Article a = new Article(
                    row.getInt("id"), row.getString("heading"), row.getString("intro"),
                    row.getString("body"), row.getInt("contentType"), row.getInt("focusId"), row.getBoolean("active")
            );
            List<FilePath> files = fileRepository.getFilesForArticle(row.getInt("id"));
            a.setFiles(files);
            return a;
        });
    }

    public Article fetchArticles(int id) {
        List<Article> articles = jdbcTemplate.query("select * from articles where id = ?", new Object[]{id},
                (row, number) -> {

                    Article a = new Article(
                            row.getInt("id"), row.getString("heading"), row.getString("intro"),
                            row.getString("body"), row.getInt("contentType"), row.getInt("focusId"), row.getBoolean("active")
                    );

                    List<FilePath> files = fileRepository.getFilesForArticle(row.getInt("id"));
                    a.setFiles(files);
                    System.out.println(files);
                    return a;
                });


        if (articles.size() > 0) {
            return articles.get(0);
        } else {
            return null;
        }
    }


    public void removeArticle(int id) {
        jdbcTemplate.update("delete from filePath where articleId = ?", id);
        jdbcTemplate.update("delete from articles where id = ?", id);
    }


      public Article addArticle(Article a) {
        KeyHolder holder = new GeneratedKeyHolder();

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("heading", a.getHeading())
                .addValue("intro", a.getIntro())
                .addValue("body", a.getBody())
                .addValue("contentType", a.getType())
                .addValue("focusId", a.getFocusId())
                .addValue("active", a.isActive());
        namedParameterJdbcTemplate.update(INSERT_SQL, parameters, holder);
        a.setId(holder.getKey().intValue());
        return a;
    }


    public void updateArticle(Article a) {
        jdbcTemplate.update("update articles set `heading` =  ?, `intro` = ?, `body` = ?, `contentType` = ?, `focusId` = ?, `active` = ? where id = ?", a.getHeading(),
                a.getIntro(), a.getBody(), a.getType(), a.getFocusId(), a.isActive(), a.getId());
    }

    public void updateFocusId(Article a) {
        jdbcTemplate.update("update articles set focusId = 0 where focusId = ?", a.getFocusId());
    }
}
