package ee.koertekoolsporus.kortekoolsoprus_api.repository;

import ee.koertekoolsporus.kortekoolsoprus_api.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    private  String INSERT_SQL = "insert into user (username, password) values(:username, :password)";

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public boolean userExists(String username) {
        Integer count = jdbcTemplate.queryForObject(
                "select count(id) from user where username = ?",
                new Object[]{username},
                Integer.class
        );
        return count != null && count > 0;
    }

    public User addUser(User u) {
        KeyHolder holder = new GeneratedKeyHolder();

        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("username", u.getUsername())
                .addValue("password", u.getPassword());
        namedParameterJdbcTemplate.update(INSERT_SQL, parameters, holder);
        u.setId(holder.getKey().longValue());
        addRole(u);
        return u;
    }

    public void addRole(User u) {
        jdbcTemplate.update("insert into user_role (`user_id`, `role_id`) values (?, ?)", u.getId(), 1);
    }


    public User getUserByUsername(String username) {
        List<User> users = jdbcTemplate.query(
                "select * from `user` where `username` = ?",
                new Object[]{username},
                (rs, rowNum) -> new User(rs.getLong("id"), rs.getString("username"), rs.getString("password"), getUserRoles(rs.getInt("id")))
        );
        return users.size() > 0 ? users.get(0) : null;
    }

    public List<String> getUserRoles(int userId) {
        return jdbcTemplate.queryForList("select r.`role` from `role` r inner join `user_role` ur on r.id = ur.role_id where ur.user_id = ?", new Object[]{userId}, String.class);
    }

}
