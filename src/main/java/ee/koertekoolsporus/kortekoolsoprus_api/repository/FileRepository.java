package ee.koertekoolsporus.kortekoolsoprus_api.repository;

import ee.koertekoolsporus.kortekoolsoprus_api.model.FilePath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class FileRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void addFilePath(String fileName, String fileLocation, int id) {
        jdbcTemplate.update("insert into filePath (`generatedFileName`, `targetLocation`, `articleId`) values (?, ?, ?)", fileName, fileLocation, id);
    }

    public void removeFilePath(String fileName) {
        jdbcTemplate.update("delete from filePath where generatedFileName = ?", fileName);
    }

    public List<FilePath> getFilesForArticle(int id) {
        List<FilePath> files = jdbcTemplate.query("select * from filePath where articleId = ?", new Object[]{id},
                (row, number) -> {

                    FilePath filePath = new FilePath(
                            row.getString("generatedFileName"), row.getString("targetLocation")
                    );
                    return filePath;
                });
        return files;
    }

}


