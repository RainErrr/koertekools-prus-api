package ee.koertekoolsporus.kortekoolsoprus_api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class KortekoolsoprusApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(KortekoolsoprusApiApplication.class, args);
	}



}
