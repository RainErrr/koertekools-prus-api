package ee.koertekoolsporus.kortekoolsoprus_api.dto;

import java.util.ArrayList;
import java.util.List;

public class LoginResponseDto {
    private final Long id;
    private final String username;
    private final String token;
    private  List<String> roles = new ArrayList<>();

    public LoginResponseDto(Long id, String username, String token, List<String> roles) {
        this.id = id;
        this.username = username;
        this.token = token;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getToken() {
        return this.token;
    }

    public List<String> getRoles() {
        return roles;
    }
}
