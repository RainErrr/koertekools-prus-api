package ee.koertekoolsporus.kortekoolsoprus_api.dto;

public class RegisterResponseDto {
    private Boolean success;

    public RegisterResponseDto(Boolean success) {
        this.success = success;
    }

    public RegisterResponseDto() {
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
