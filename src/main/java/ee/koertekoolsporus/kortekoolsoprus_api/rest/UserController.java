package ee.koertekoolsporus.kortekoolsoprus_api.rest;

import ee.koertekoolsporus.kortekoolsoprus_api.dto.LoginRequestDto;
import ee.koertekoolsporus.kortekoolsoprus_api.dto.LoginResponseDto;
import ee.koertekoolsporus.kortekoolsoprus_api.dto.RegisterRequestDto;
import ee.koertekoolsporus.kortekoolsoprus_api.dto.RegisterResponseDto;
import ee.koertekoolsporus.kortekoolsoprus_api.repository.UserRepository;
import ee.koertekoolsporus.kortekoolsoprus_api.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/users")
@CrossOrigin("*")
@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @PostMapping("/register")
    public RegisterResponseDto registerUser(@RequestBody RegisterRequestDto registerRequestDto){
        Boolean success = userService.register(registerRequestDto);
        return new RegisterResponseDto(success);
        //siin voiks olla errori tagastamise loogika, service ei pea seda teadma
    }

    @PostMapping("/login")
    public LoginResponseDto authenticate(@RequestBody LoginRequestDto request) throws Exception {
        return userService.authenticate(request);
    }

}
