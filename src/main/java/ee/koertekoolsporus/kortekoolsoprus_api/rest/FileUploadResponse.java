package ee.koertekoolsporus.kortekoolsoprus_api.rest;

public class FileUploadResponse {
    private String fileName;
    private String url;
    private String fileType;
    private long size;

    public FileUploadResponse(String imageName, String url, String fileType, long size) {
        this.fileName = imageName;
        this.url = url;
        this.fileType = fileType;
        this.size = size;
    }

    public FileUploadResponse() {

    }

    public String getImageName() {
        return fileName;
    }

    public void setImageName(String imageName) {
        this.fileName = imageName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }
}
