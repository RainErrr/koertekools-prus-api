package ee.koertekoolsporus.kortekoolsoprus_api.rest;

import ee.koertekoolsporus.kortekoolsoprus_api.model.Article;
import ee.koertekoolsporus.kortekoolsoprus_api.repository.ArticleRepository;
import ee.koertekoolsporus.kortekoolsoprus_api.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;

@RestController
@RequestMapping("/articles")
@CrossOrigin("*")

public class ArticleController {

    @Autowired
    private ArticleRepository articleRepository;

    @Autowired
    private ArticleService articleService;

    @GetMapping
    public List<Article> fetchArticles() {
        return articleRepository.fetchArticles();
    }

    @GetMapping("/active")
    public List<Article> fetchActiveArticles(boolean active) {
        return articleRepository.fetchActiveArticles(active);
    }

    @GetMapping("/{id}")
    public Article giveMeSingleArticle(@PathVariable("id") int id) {
        return articleRepository.fetchArticles(id);
    }

    @DeleteMapping("/{id}")
    public void removeArticle(@PathVariable("id") int id){
        articleService.removeArticle(id);
    }

    @PostMapping
    public int addArticle(@RequestBody Article a){
        return articleService.updateOrAddArticle(a);
    }

    @PutMapping
    public int updateArticle(@RequestBody Article a){
        return articleService.updateOrAddArticle(a);
    }
}
