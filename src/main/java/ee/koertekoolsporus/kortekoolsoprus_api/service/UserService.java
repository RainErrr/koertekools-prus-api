package ee.koertekoolsporus.kortekoolsoprus_api.service;

import ee.koertekoolsporus.kortekoolsoprus_api.dto.LoginRequestDto;
import ee.koertekoolsporus.kortekoolsoprus_api.dto.LoginResponseDto;
import ee.koertekoolsporus.kortekoolsoprus_api.dto.RegisterRequestDto;
import ee.koertekoolsporus.kortekoolsoprus_api.model.User;
import ee.koertekoolsporus.kortekoolsoprus_api.repository.UserRepository;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private JwtTokenService jwtTokenService;
    private AuthenticationManager authenticationManager;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtTokenService jwtTokenService, AuthenticationManager authenticationManager) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenService = jwtTokenService;
        this.authenticationManager = authenticationManager;
    }

    public Boolean register(RegisterRequestDto registerRequestDto) {
        User user = new User(registerRequestDto.getUsername(), passwordEncoder.encode(registerRequestDto.getPassword()));
        if (!userRepository.userExists(registerRequestDto.getUsername())) {
            userRepository.addUser(user);
            return true;
        } else {
            return false;
        }
    }

    public LoginResponseDto authenticate(LoginRequestDto request) throws Exception {
        authenticate(request.getUsername(), request.getPassword() );
        final User userDetails = userRepository.getUserByUsername(request.getUsername());
        final String token = jwtTokenService.generateToken(userDetails.getUsername());
        return new LoginResponseDto(userDetails.getId(), userDetails.getUsername(), token, userDetails.getRoles());
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
