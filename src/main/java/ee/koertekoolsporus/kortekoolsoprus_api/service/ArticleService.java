package ee.koertekoolsporus.kortekoolsoprus_api.service;


import ee.koertekoolsporus.kortekoolsoprus_api.model.Article;
import ee.koertekoolsporus.kortekoolsoprus_api.repository.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;

@Service
public class ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    public void removeArticle(int id) {
        Assert.isTrue(id > 0, "Ei õnnestu kustutada artiklit, millel puudub ID.");
        Assert.notNull(articleRepository.fetchArticles(id), "Artiklit ei õnnestunud leida!");
        articleRepository.removeArticle(id);
    }

    public int updateOrAddArticle(Article a) {
        //Assert.notNull(a, "Artiklit ei leitud!");
        Assert.isTrue(a.getHeading() != null, "Artikli pealkiri sisestamata!");
        Assert.isTrue(a.getIntro() != null, "Artikli sissejuhatus sisestamata!");
        Assert.isTrue(a.getBody() != null, "Artiklil puudub sisu!");

        if (a.getFocusId() > 0) {
            articleRepository.updateFocusId(a);
        }


        if (a.getId() > 0) {
            articleRepository.updateArticle(a);
            return  a.getId();

        } else {
            articleRepository.addArticle(a);
            return a.getId();
        }

    }
}
